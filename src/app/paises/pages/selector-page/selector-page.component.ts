import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { PaisSmall, Pais } from '../../interfaces/paises.interfaces';
import { PaisesServiceService } from '../../services/paises-service.service';
import { switchMap, tap } from 'rxjs/operators';

@Component({
  selector: 'app-selector-page',
  templateUrl: './selector-page.component.html',
  styleUrls: ['./selector-page.component.css'],
})
export class SelectorPageComponent implements OnInit {
  miFormulario: FormGroup = this._fb.group({
    region: ['', Validators.required],
    pais: ['', Validators.required],
    // {value: '', disabled: true}, desabilitar en el formulario si el valor es vacio
    frontera: ['', Validators.required],
  });

  // llenar selectores
  regiones: string[] = [];
  paises: PaisSmall[] = [];
  fronteras: PaisSmall[] = [];

  // UI
  cargando: boolean = false;

  constructor(
    private _fb: FormBuilder,
    private _paisesServices: PaisesServiceService
  ) {}

  ngOnInit(): void {
    this.regiones = this._paisesServices.regiones;

    // cuando cambie la region
    // se puede hacer de esta forma tambien
    /* this.miFormulario.get('region')?.valueChanges.subscribe(
      response => {
        this._paisesServices.getPaisesRegion(response).subscribe(
          response => {
            this.paises = response;
            console.log(this.paises);
          }
        );
      }
    ); */
    this.miFormulario
      .get('region')
      ?.valueChanges.pipe(
        // obtener opcion selecciona en el select
        tap((response) => {
          // resetea el valor del select de pais cuando cambia el select de region
          this.miFormulario.get('pais')?.reset('');
          this.cargando = true;
        }),
        // permite emitir el valor de otro observable
        switchMap((response) => this._paisesServices.getPaisesRegion(response))
      )
      .subscribe((response) => {
        this.paises = response;
        this.cargando = false;
      });

    // cuando cambia el select de pais
    this.miFormulario
      .get('pais')
      ?.valueChanges.pipe(
        tap((response) => {
          this.miFormulario.get('frontera')?.reset('');
          this.cargando = true;
        }),
        // modificamos la respuesta
        switchMap((response) =>
          this._paisesServices.getPaisPorAlphaCode(response)
        ),
        // volvemos a modificar la respuesta nuevamente
        switchMap((pais) =>
          this._paisesServices.getPaisesPorCodigos(pais?.borders!)
        )
      )
      .subscribe((response) => {
        // si la respuesta no existe que retorne un valor null
        this.fronteras = response;
        console.log(response);
        this.cargando = false;
      });
  }

  guardar() {
    console.log(this.miFormulario.value);
  }
}
