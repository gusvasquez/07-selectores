import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Pais, PaisSmall } from '../interfaces/paises.interfaces';
import { Observable, of, combineLatest } from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class PaisesServiceService {

  private _baseUrl: string = 'https://restcountries.com/v2';

  private _regiones: string[] = [
    'Africa',
    'Americas',
    'Asia',
    'Europe',
    'Oceania',
  ];

  get regiones(): string[]{
    // al desestructurarlo crea una copia del arreglo que se esta retornando
    return [...this._regiones];
  }

  constructor(private _http: HttpClient) {}

  getPaisesRegion(region: string): Observable<PaisSmall[]>{
    const url = `${ this._baseUrl}/region/${region}?fields=name,alpha3Code`;
    return this._http.get<PaisSmall[]>(url);
  }

  // la funcion puede retornare algun valor de tipo Pais o null
  getPaisPorAlphaCode(codigo: string): Observable<Pais | null>{

    if(!codigo){
      // off nos permite retornar un nuevo observable
      return of(null);
    }

    const url = `${ this._baseUrl}/alpha/${codigo}`;
    return this._http.get<Pais>(url);
  }

  getPaisPorAlphaCodeSmall(codigo: string): Observable<PaisSmall>{

    const url = `${ this._baseUrl}/alpha/${codigo}?fields=name,alpha3Code`;
    return this._http.get<Pais>(url);
  }

  getPaisesPorCodigos(borders: string[]): Observable<PaisSmall[]>{
    if(!borders){
      return of([]);
    }

    // creamos un arreglo de peticiones
    const peticiones: Observable<PaisSmall>[] = [];

    borders.forEach(codigo => {
      // se almacena las peticiones en la constante peticion
      const peticion = this.getPaisPorAlphaCodeSmall(codigo);
      // se agregan las peticiones en el arrelgo de observables de tipo PaisSamll
      peticiones.push(peticion);
    });

    // comineLatest devuelve un observable
    return combineLatest(peticiones);

  }

}
